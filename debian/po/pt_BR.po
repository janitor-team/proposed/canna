#
#    Translators, if you are not familiar with the PO format, gettext
#    documentation is worth reading, especially sections dedicated to
#    this format, e.g. by running:
#         info -n '(gettext)PO Files'
#         info -n '(gettext)Header Entry'
#
#    Some information specific to po-debconf are available at
#            /usr/share/doc/po-debconf/README-trans
#         or http://www.debian.org/intl/l10n/po-debconf/README-trans
#
#    Developers do not need to manually edit POT or PO files.
#
msgid ""
msgstr ""
"Project-Id-Version: canna_3.7-3\n"
"Report-Msgid-Bugs-To: canna@packages.debian.org\n"
"POT-Creation-Date: 2009-05-03 07:53+0200\n"
"PO-Revision-Date: 2004-01-12 18:46-0300\n"
"Last-Translator: Andre Luis Lopes <andrelop@debian.org>\n"
"Language-Team: Debian-BR Project <debian-l10n-portuguese@lists.debian.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=ISO-8859-1\n"
"Content-Transfer-Encoding: 8bit\n"

#. Type: boolean
#. Description
#: ../templates:2001
msgid "Should the Canna server run automatically?"
msgstr ""

#. Type: boolean
#. Description
#: ../templates:2001
#, fuzzy
#| msgid ""
#| "This package contains the Canna server and server-related utilities. If "
#| "you are only interested in these utilities, you can disable the Canna "
#| "server here."
msgid ""
"This package contains the Canna server and server-related utilities. If you "
"are only interested in these utilities, you can disable the Canna server now."
msgstr ""
"Este pacote contém o servidor Canna e os utilitários de servidor "
"relacionados. Caso você esteja somente interessado nos utilitários, você "
"pode desabilitar o servidor Canna aqui."

#. Type: boolean
#. Description
#: ../templates:3001
msgid "Should the Canna server run in network mode?"
msgstr ""

#. Type: boolean
#. Description
#: ../templates:3001
msgid ""
"By default the Canna server will run without support for network "
"connections, and will only accept connections on UNIX domain sockets, from "
"clients running on the same host."
msgstr ""

#. Type: boolean
#. Description
#: ../templates:3001
msgid ""
"If you choose this option, network support will be activated, and the Canna "
"server will accept connections on TCP sockets from clients that may be on "
"remote hosts. Some clients (such as egg and yc-el) require this mode even if "
"they run on the local host."
msgstr ""

#. Type: boolean
#. Description
#: ../templates:4001
#, fuzzy
#| msgid "Manage /etc/hosts.canna with debconf ?"
msgid "Manage /etc/hosts.canna automatically?"
msgstr "Gerenciar o arquivo /etc/hosts.canna com o debconf ?"

#. Type: boolean
#. Description
#: ../templates:4001
#, fuzzy
msgid ""
"The /etc/hosts.canna file lists hosts allowed to connect to the Canna server."
msgstr ""
"O arquivo /etc/hosts.canna controla quais hosts podem se conectar a este "
"servidor."

#. Type: boolean
#. Description
#: ../templates:4001
msgid ""
"You should not accept this option if you prefer managing the file's contents "
"manually."
msgstr ""

#. Type: string
#. Description
#: ../templates:5001
#, fuzzy
msgid "Hosts allowed to connect to this Canna server:"
msgstr ""
"Por favor, informe o nome dos hosts que tem permissão para se conectarem a "
"este servidor"

#. Type: string
#. Description
#: ../templates:5001
#, fuzzy
msgid ""
"Please enter the names of the hosts allowed to connect to this Canna server, "
"separated by spaces."
msgstr ""
"Por favor informe os nomes dos hosts, separados por vírgulas, que tem "
"permissão para se conectarem a este servidor Canna."

#. Type: string
#. Description
#: ../templates:5001
#, fuzzy
msgid "You can use \"unix\" to allow access via UNIX domain sockets."
msgstr ""
"`unix' é uma notação especial que significa permitir o acesso através de "
"soquetes de domínio UNIX."

#. Type: select
#. Description
#: ../libcanna1g.templates:2001
#, fuzzy
#| msgid "Canna input style"
msgid "Canna input style:"
msgstr "Estilo de entrada Canna"

#. Type: select
#. Description
#: ../libcanna1g.templates:2001
#, fuzzy
msgid ""
"Please choose the default Canna input style:\n"
" verbose: Canna3.5 default style with verbose comments;\n"
" 1.1    : old Canna style (ver. 1.1);\n"
" 1.2    : old Canna style (ver. 1.2);\n"
" jdaemon: jdaemon style;\n"
" just   : JustSystems ATOK style;\n"
" lan5   : LAN5 style;\n"
" matsu  : Matsu word processor style;\n"
" skk    : SKK style;\n"
" tut    : TUT-Code style;\n"
" unix   : UNIX style;\n"
" vje    : vje style;\n"
" wx2+   : WX2+ style."
msgstr ""
" verbose - Estilo padrão Canna3.5 com comentários verbosos\n"
" 1.1     - antigo estilo Canna (ver. 1.1)\n"
" 1.2     - antigo estilo Canna (ver. 1.2)\n"
" jdaemon - estilo jdaemon\n"
" just    - estilo parecido com Justsystem ATOK\n"
" lan5    - estilo parecido com LAN5\n"
" matsu   - estilo parecido com processador de textos MATSU\n"
" skk     - estilo parecido com SKK\n"
" tut     - para TUT-Code\n"
" unix    - estilo UNIX\n"
" vje     - estilo parecido com vje\n"
" wx2+    - estilo WX2+"

#~ msgid "Do you want to run the Canna server ?"
#~ msgstr "Deseja executar o servidor Canna ?"

#, fuzzy
#~ msgid "Do you want to connect to the Canna server from a remote host?"
#~ msgstr "Conectar-se ao servidor Canna a partir de um host remoto ?"

#, fuzzy
#~ msgid ""
#~ "The Canna server only allows connections via UNIX domain sockets when the "
#~ "`-inet' option is not specified.  This means that when the Canna server "
#~ "is started without the `-inet' option, only clients which run on the same "
#~ "host can connect to the server. Some clients such as, `egg' and `yc-el', "
#~ "do not support UNIX domain socket and require the `-inet' option, even if "
#~ "they run on the same host."
#~ msgstr ""
#~ "Esta versão do servidor Canna permite conexões somente através de um "
#~ "soquete de domínio UNIX sem a opção `-inet' (isso é um melhora em termos "
#~ "de segurança). Isso significa que quando o servidor Canna é iniciado sem "
#~ "a opção `inet', somente os clientes em execução no mesmo host onde o "
#~ "servidor Canna está em execução podem se conectar ao servidor. Note que "
#~ "alguns clientes como o `egg' e o `yc-el' não suportam soquetes de domínio "
#~ "UNIX e precisam da opção `-inet' mesmo quando quando são executados no "
#~ "mesmo host."

#, fuzzy
#~ msgid ""
#~ "If you want to connect to this Canna server from remote hosts, or if you "
#~ "want to use INET-domain-only software, you should run the server with the "
#~ "`-inet' option."
#~ msgstr ""
#~ "Caso você queira se conectar ao servidor Canna a partir de hosts remotos "
#~ "ou caso você queira usar softwares somente-domínios-INET você deverá "
#~ "executar o servidor com a opção `-inet'."

#~ msgid ""
#~ "By default /etc/hosts.canna will be managed with debconf. Refuse here if "
#~ "you want to manage /etc/hosts.canna yourself."
#~ msgstr ""
#~ "Por padrão, o arquivo /etc/hosts.canna será gerenciado com o debconf. Não "
#~ "aceite a opção de gerenciar o arquivo /etc/hosts.canna com o debconf caso "
#~ "você queira gerenciar esse arquivo manualmente."

#, fuzzy
#~ msgid ""
#~ "Please choose the default canna input style below, which will be "
#~ "installed if there is no previous configuration:"
#~ msgstr ""
#~ "Por favor, escolha o método de entrada padrão do Canna abaixo, o qual "
#~ "será instalado caso não exista configuração anterior :"

#~ msgid "Please choose default canna input style below:"
#~ msgstr "Por favor, escolha o estilo de entrada canna padrão abaixo :"
